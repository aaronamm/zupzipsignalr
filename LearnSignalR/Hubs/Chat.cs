﻿using Microsoft.AspNet.SignalR;

namespace LearnSignalR.Hubs
{
    public class Chat : Hub
    {

        public void BroadcastMessage(string username, string message)
        {

            Clients.All.receiveMessage(username+ " : "+message);

        }

    }
}